package dateiumnenner;

import java.awt.Color;
import javax.swing.JDialog;
import javax.swing.JLabel;

/*
 * Dateiumnenner kann beliebig viele Dateien so umbenennen, dass am Anfang des neuen Namens das Datum der letzten Aenderung steht,
 * danach ein selbst gewaehlter Text und die letzte Dateierweiterung. 
 * 
 * Programmierer Webseite: http://www.t-brieskorn.de
 * */
public class About {
	static JDialog about() {
		final JDialog aboutFrame = new JDialog();
		aboutFrame.setBounds(10, 45, 400, 200);
		aboutFrame.setTitle("about Dateiumbenenner");
		aboutFrame.setBackground(Color.LIGHT_GRAY);
		aboutFrame.setLayout(null);

		// Developer
		JLabel developerL = new JLabel("Programmierer: Torsten Brieskorn");
		aboutFrame.add(developerL);
		developerL.setBounds(5, 5, 280, 30);

		// Developer Webseite
		JLabel developerW = new JLabel("http://www.t-brieskorn.de");
		aboutFrame.add(developerW);
		developerW.setBounds(5, 35, 280, 30);

		aboutFrame.setVisible(true);
		return aboutFrame;
	}

}