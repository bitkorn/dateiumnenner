package dateiumnenner;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.JFileChooser;

/*
 * Dateiumnenner kann beliebig viele Dateien so umbenennen, dass am Anfang des neuen Namens das Datum der letzten Aenderung steht,
 * danach ein selbst gewaehlter Text und die letzte Dateierweiterung. 
 * 
 * Programmierer Webseite: http://www.t-brieskorn.de
 * */
public class ReadFile {

    private static ReadFile readDatei;
    Datei d = new Datei();

    private int anz;
    private String[] fileList;
    private String[] oldNames;
    private long[] fileDatesLong;
    private String[] fileDatesString;

    private final Locale[] loc = new Locale[]{Locale.GERMANY, Locale.UK};
    File[] files;

    // Singleton Konstruktor
    private ReadFile() {
    }

    /*
	 * initialisiert fileList, oldNames
     */
    private void initFiles() {
        files = d.getDateien(); // das File Array aus dem Dateiwaehler
        anz = files.length; // Anzahl der Files
        fileList = new String[anz];
        fileDatesLong = new long[anz];
        fileDatesString = new String[anz];
        oldNames = new String[anz];
        if (anz > 0) {
            for (int i = 0; i < anz; i++) {
                fileList[i] = files[i].getName(); // liefert Strings zurueck
            }
            for (int i = 0; i < anz; i++) {
                oldNames[i] = fileList[i].substring(0, fileList[i]
                        .lastIndexOf('.'));
                System.out.println(i + ". alter Name: " + oldNames[i]);
            }
        }
    }

    /*
	 * liefert die File list muss als erstes augerufen werden weil hier
	 * initFiles() aufgerufen wird
     */
    String[] getFileNames() {
        initFiles();
        return fileList;
    }

    /*
	 * Datum der Dateien holen, aus dem Umnenner aufgerufen
     */
    String[] getDateAsString() {

        if (anz > 0) {

            for (int i = 0; i < anz; i++) {
                fileDatesLong[i] = files[i].lastModified();
            }
            DateFormat sdf = new SimpleDateFormat("yyyyMMddkkmm", loc[0]);

            for (int i = 0; i < anz; i++) {
                fileDatesString[i] = sdf.format(fileDatesLong[i]);
            }
        }
        return fileDatesString;
    }

    /*
	 * die alten Dateinamen holen
     */
    String[] getOldNames() {
        return oldNames;
    }

    /*
	 * die Dateinamen f�r die Anzeige holen, aus der GUI aufgerufen
     */
    String getNamesAsFliesstext() {
        String[] names = getFileNames();
        String namesOut = "";

        for (int i = 0; i < anz; i++) {
            namesOut += names[i] + "\n";
        }
        return namesOut;
    }

    public int getAnz() {
        return anz;
    }

    // Factory Methode
    public static ReadFile getInstance() {
        if (readDatei == null) {
            readDatei = new ReadFile();
            return readDatei;
        } else {
            return readDatei;
        }
    }
}

/*
 * Klasse mit dem File Array und Dateiw�hldialog
 */
class Datei {

    private File[] dateien;

    // oeffnet einen Dialog zum Dateien auswaehlen
    // ausgelost vom offnen Button in der GUI, mittels ReadDatei
    // initialisiert Variable dateien
    void dateiWaehler() {

        JFileChooser dateiWahl = new JFileChooser();
        dateiWahl.setMultiSelectionEnabled(true);
        // File oeffnen Dialog
        int returnVal = dateiWahl.showOpenDialog(dateiWahl);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            dateien = dateiWahl.getSelectedFiles();
        }
    }

    // gibt die geoeffneten Dateien zurueck
    File[] getDateien() {
        return dateien;
    }
}
