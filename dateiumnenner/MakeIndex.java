package dateiumnenner;

/*
 * Dateiumnenner kann beliebig viele Dateien so umbenennen, dass am Anfang des neuen Namens das Datum der letzten Aenderung steht,
 * danach ein selbst gewaehlter Text und die letzte Dateierweiterung. 
 * 
 * Programmierer Webseite: http://www.t-brieskorn.de
 * */
public class MakeIndex {

    public static String makeI(int index) {
        String zAusgabe = "";
        int eingabeI = index;

//		System.out.println("Index: " + eingabeI);
        if (eingabeI <= 9) {
            zAusgabe = "00" + String.valueOf(eingabeI);
        }

        if (eingabeI > 9 && eingabeI < 100) {
            zAusgabe = "0" + String.valueOf(eingabeI);
        }

        if (eingabeI > 99 && eingabeI < 1000) {
            zAusgabe = String.valueOf(eingabeI);
        }

        return zAusgabe;
    }

}
