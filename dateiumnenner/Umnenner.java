package dateiumnenner;

import java.io.File;

import javax.swing.JOptionPane;

/*
 * Dateiumnenner kann beliebig viele Dateien so umbenennen, dass am Anfang des neuen Namens das Datum der letzten Aenderung steht,
 * danach ein selbst gewaehlter Text und die letzte Dateierweiterung. 
 * 
 * Programmierer Webseite: http://www.t-brieskorn.de
 * 
 * */
public class Umnenner {

    private static Umnenner umnenner;
    private ReadFile readFile = ReadFile.getInstance();
    private Character[] sonderzeichen = {'"', '<', '>', '/', '\\', '|', '*', '?'};
    private int anz;

    // Singleton
    private Umnenner() {
    }

    // hole Dateianhang, indirekt aus der GUI aufgerufen
    private String[] getDateianhang() {
        anz = readFile.getAnz();
        String[] names = readFile.getFileNames();
        int[] datIndex = new int[anz];
        final String[] datAnh = new String[anz];

        if (names.length > 0) {
            datIndex[0] = names[0].lastIndexOf('.');
        }
        for (int i = 0; i < anz; i++) {
            datIndex[i] = names[i].lastIndexOf('.');
        }

        if (names.length > 0) {
            datAnh[0] = names[0].substring(datIndex[0]);
        }
        for (int i = 0; i < anz; i++) {
            datAnh[i] = names[i].substring(datIndex[i]);
        }

        return datAnh;
    }

    /*
	 * Umbenennen, aus der GUI aufgerufen
     */
    boolean benenneUm(String dAnh) {
        String[] datDates = readFile.getDateAsString();
        String text = dAnh;
        String[] datAnh = getDateianhang();
        String[] newName = new String[anz];
        File[] oldFiles = readFile.d.getDateien();
        File[] newFiles = new File[anz];
        boolean erfmeld = false;

        if (!testSonderzeichen(text)) {
            return false;
        }
        /*
		 * Fenster um den Speicherort festzulegen
         */
        String verzeichnis = new ReadDirectory().getDirectory();

        // neue Namen erstellen
        Dgui dgui = Dgui.getInstance();
        for (int i = 0; i < anz; i++) {
            String index = (dgui.isIndexChecked()) ? MakeIndex.makeI(i) + "_"
                    : "";
            String date = (dgui.isDateChecked()) ? datDates[i] + "_" : "";
            String oldName = (dgui.isOldNamesChecked()) ? readFile
                    .getOldNames()[i] : "";
            newName[i] = verzeichnis + index + date + text + oldName
                    + datAnh[i];
            System.out.println("neuer Name für " + i + ": " + newName[i]);
        }

        // neue LEERE Files erstellen
        for (int i = 0; i < anz; i++) {
            newFiles[i] = new File(newName[i]);
        }

        // alte Files zu neuen Files schieben
        for (int i = 0; i < anz; i++) {
            erfmeld = oldFiles[i].renameTo(newFiles[i]);
        }
        return erfmeld;
    }

    private boolean testSonderzeichen(String string) {
        for (int i = 0; i < sonderzeichen.length; i++) {
            if (string.contains(sonderzeichen[i].toString())) {
                JOptionPane
                        .showMessageDialog(
                                null,
                                "Keine der folgenden Zeichen benutzen: \", <, >, /, \\, *, ?, |",
                                "Achtung", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        return true;
    }

    // Factory Methode
    public static Umnenner getInstance() {
        if (umnenner == null) {
            umnenner = new Umnenner();
            return umnenner;
        } else {
            return umnenner;
        }
    }

}
