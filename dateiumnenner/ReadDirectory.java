package dateiumnenner;

import java.io.File;

import javax.swing.JFileChooser;

public class ReadDirectory {

    File verzeichnis;

    String getDirectory() {
        JFileChooser verzWahl = new JFileChooser();
        verzWahl.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = verzWahl.showSaveDialog(verzWahl); // blockierende Methode
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            verzeichnis = verzWahl.getSelectedFile();
        }
        System.out.println(verzeichnis);
        // ob das mit dem Slash bei Windoof immer so gut ist
        // auch mal unter Linux testen
        // bei Windoof funzt es
        return verzeichnis.getAbsolutePath() + "/";
    }
}
