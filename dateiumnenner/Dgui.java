package dateiumnenner;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * Dateiumnenner kann beliebig viele Dateien so umbenennen, dass am Anfang des neuen Namens das Datum der letzten Aenderung steht,
 * danach ein selbst gewaehlter Text und die letzte Dateierweiterung. 
 * 
 * Programmierer Webseite: http://www.t-brieskorn.de
 * 
 * */
public class Dgui {

    private static Dgui dgui;
    private final ReadFile readDatei = ReadFile.getInstance();
    private final Umnenner umnenner = Umnenner.getInstance();
    private boolean isIndexChecked;
    private boolean isDateChecked;
    private boolean isOldNamesChecked;

    // Singleton
    private Dgui() {
    }

    void gui() {

        JFrame umnennFrame = new JFrame("Datei-Umnenner");
        umnennFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        umnennFrame.setSize(800, 460);
        umnennFrame.setResizable(false);
        umnennFrame.setBackground(Color.LIGHT_GRAY);
        umnennFrame.setLayout(null);
        int fSize = umnennFrame.getWidth();
        int tSize = 280;

        // Komponenten Menu
        JMenuBar menuBar = new JMenuBar();

        // Datei Menu
        final JMenu fileMenu = new JMenu("Datei");
        menuBar.add(fileMenu);

        // Hilfe Menu
        JMenu helpMenu = new JMenu("Hilfe");
        menuBar.add(helpMenu);

        // About Dialog
        JMenuItem aboutItem = new JMenuItem("about");
        class AboutAction implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                About.about();
            }
        }
        aboutItem.addActionListener(new AboutAction());
        helpMenu.add(aboutItem);

        // TextArea mit den geoeffneten Dateinamen
        final JTextArea datL = new JTextArea(tSize, 200);
        datL.setEditable(false);
        JScrollPane dateienS = new JScrollPane(datL);
        dateienS.setBounds(fSize - 300, 70, tSize, 330);

        /*
		 * *********************Actions********************
         */
        // oeffnen Action
        JMenuItem oeffneItem = new JMenuItem("Dateien öffnen");
        class OeffneAction implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                readDatei.d.dateiWaehler();
                if (readDatei.d.getDateien() != null
                        && readDatei.d.getDateien().length > 0) {
                    String fl = readDatei.getNamesAsFliesstext();
                    datL.setText(fl);
                } else {
                    datL.setText("keine Dateien geöffnet");
                }
            }
        }
        OeffneAction oeffneAction = new OeffneAction();
        oeffneItem.addActionListener(oeffneAction);
        fileMenu.add(oeffneItem);

        // Schliessen Action
        JMenuItem schliessenItem = new JMenuItem("schliessen");
        class SchliessenAction implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        }
        schliessenItem.addActionListener(new SchliessenAction());
        fileMenu.add(schliessenItem);

        // Textfeld fuer Dateinamenserweiterung
        final JTextField dateierwT = new JTextField("Text", 20);
        dateierwT.setBounds(20, 290, 200, 30);

        // Ereignisempfaenger fuer Umbenennen Button
        Action umnennenAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            {
                putValue(Action.NAME, "umbenennen");
                putValue(Action.ACCELERATOR_KEY, 0);
            }

            public void actionPerformed(ActionEvent e) {
                String text = dateierwT.getText();
                boolean b = umnenner.benenneUm(text);
                if (b == true) {
                    datL.append("\nDateien erfolgreich umbenannt!");
                } else {
                    datL.append("\nEs ist ein Fehler aufgetreten!");
                }
            }
        };

        // Klasse f�r checkIndex
        class ButtonCheckIndexListener implements ItemListener {

            @Override
            public void itemStateChanged(ItemEvent e) {
                int zustand = e.getStateChange();
                if (zustand == ItemEvent.SELECTED) {
                    isIndexChecked = true;
                } else {
                    isIndexChecked = false;
                }
            }
        }
        ;

        // Klasse f�r checkDate
        class ButtonCheckDateListener implements ItemListener {

            @Override
            public void itemStateChanged(ItemEvent e) {
                int zustand = e.getStateChange();
                if (zustand == ItemEvent.SELECTED) {
                    isDateChecked = true;
                } else {
                    isDateChecked = false;
                }
            }
        }
        ;

        // Klasse f�r checkOldNames
        class ButtonCheckOldNamesListener implements ItemListener {

            @Override
            public void itemStateChanged(ItemEvent e) {
                int zustand = e.getStateChange();
                if (zustand == ItemEvent.SELECTED) {
                    isOldNamesChecked = true;
                } else {
                    isOldNamesChecked = false;
                }
            }

        }
        /*
		 * ************** Label Label Label Label**********************
         */
        JLabel infoLab01 = new JLabel("Dateien die umbenannt werden:");
        infoLab01.setBounds(fSize - 300, 40, 280, 20);
        JLabel errorWarnLabel = new JLabel(
                "Gleichnamige Dateien im selben Ordner erzeugen einen Error!");
        errorWarnLabel.setBounds(20, 5, 400, 20);
        JLabel infoLab02 = new JLabel(
                "Das Datum der Dateien bleibt immer erhalten!");
        infoLab02.setBounds(20, 30, 400, 20);
        JLabel neuerTextLabel = new JLabel("Text der in die neuen Namen kommt:");
        neuerTextLabel.setBounds(20, 260, 310, 20);
        JLabel selbeDateWarnLabel = new JLabel(
                "ACHTUNG: Dateikopien haben das selbe Datum!");
        selbeDateWarnLabel.setBounds(20, 170, 300, 20);

        // Button zum oeffnen
        JButton oeffneButton = new JButton("öffne Dateien");
        oeffneButton.addActionListener(oeffneAction);
        oeffneButton.setBounds(fSize - 500, 80, 140, 30);

        // Checkbox: Index in den neuen Namen?
        JCheckBox checkboxIndex = new JCheckBox();
        checkboxIndex.setBounds(20, 140, 20, 20);
        checkboxIndex.addItemListener(new ButtonCheckIndexListener());
        JLabel checkIndexLabel = new JLabel("Einen Index in die neuen Namen?");
        checkIndexLabel.setBounds(50, 140, 200, 20);

        // Checkbox: Zeitstempel im neuen Namen?
        JCheckBox checkboxDate = new JCheckBox();
        checkboxDate.setBounds(20, 220, 20, 20);
        checkboxDate.addItemListener(new ButtonCheckDateListener());
        JLabel checkDateLabel = new JLabel("Datei-Datum in die neuen Namen?");
        checkDateLabel.setBounds(50, 220, 200, 20);

        // Checkbox: alte Namen in neuen Namen?
        JCheckBox checkboxOldName = new JCheckBox();
        checkboxOldName.setBounds(20, 340, 20, 20);
        checkboxOldName.addItemListener(new ButtonCheckOldNamesListener());
        JLabel checkOldNamesLabel = new JLabel("alte Namen in die neuen Namen?");
        checkOldNamesLabel.setBounds(50, 340, 200, 20);

        // Button zum umbenennen
        JButton umnennButton = new JButton("umbenennen");
        umnennButton.addActionListener(umnennenAction);
        umnennButton.setBounds(fSize - 500, 370, 140, 30);

        // in das Fenster einfuegen
        umnennFrame.setJMenuBar(menuBar);
        umnennFrame.add(infoLab01);
        umnennFrame.add(errorWarnLabel);
        umnennFrame.add(infoLab02);
        umnennFrame.add(neuerTextLabel);
        umnennFrame.add(oeffneButton);
        umnennFrame.add(checkboxIndex);
        umnennFrame.add(checkIndexLabel);
        umnennFrame.add(selbeDateWarnLabel);
        umnennFrame.add(checkboxDate);
        umnennFrame.add(checkDateLabel);
        umnennFrame.add(checkboxOldName);
        umnennFrame.add(checkOldNamesLabel);
        umnennFrame.add(umnennButton);
        umnennFrame.add(dateierwT);
        umnennFrame.add(dateienS);

        umnennFrame.setVisible(true);
    }

    public boolean isIndexChecked() {
        return isIndexChecked;
    }

    public boolean isDateChecked() {
        return isDateChecked;
    }

    public boolean isOldNamesChecked() {
        return isOldNamesChecked;
    }

    // Factory Methode
    public static Dgui getInstance() {
        if (dgui == null) {
            dgui = new Dgui();
            return dgui;
        } else {
            return dgui;
        }
    }

}
